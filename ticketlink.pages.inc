<?php

/**
 * @file
 * administration page to enable or disable ticket links and set the message indicating sales are disabled.
 */
 
function ticketlink_message($form, &$form_state) {
 	$form['ticketlink_message_text'] = array(
     '#type' => 'textarea',
     '#title' => 'Ticket Link Offline Message Text',
     '#required' => TRUE,
     '#default_value' => variable_get('ticketlink_message_text', 'Web sales are currently disabled.'),
     '#description' => t('Make sure you enable the Ticket Link Disabled Message block in your theme if you want the message to show up. No HTML is allowed other than &lt;a&gt; and &lt;br&gt;'),
   );

 	$form['ticketlink_enabled'] = array(
     '#type' => 'checkbox',
     '#title' => 'Enable Ticket Links?',
     '#default_value' => variable_get('ticketlink_enabled', 0),
   );

 	$form['submit'] = array(
 	  '#type' => 'submit',
 	  '#value' => t('Save'),
 	);

 	return $form;
}

//"Buy Tickets Now!"
//"Buy"
//'Add this performance to your shopping cart, select seats and purchase tickets.'
//'See a list of all performances for this show.'
//field_on_sale_date
//field_eventstart vs field_performance_date
//field_performances
 
function ticketlink_message_submit($form, &$form_state) {
	variable_set('ticketlink_message_text', $form_state['values']['ticketlink_message_text']);
	variable_set('ticketlink_enabled', $form_state['values']['ticketlink_enabled']);
	
	if($form_state['values']['ticketlink_enabled']) {
		drupal_set_message(t('The Ticket Links are now enabled.'));
	}
	else {
		drupal_set_message(t('The Ticket Links are now disabled.'));
	}
}